document.addEventListener("scroll", scrollEvent);

const arrowElement = document.getElementById("arrowEl");
const formBtn = document.getElementById("form-button");

arrowElement.addEventListener('click',handleScroll);
formBtn.addEventListener('click',handleFormClick);

function handleFormClick(e){
	e.preventDefault();
	alert('React > rest');
}

function handleScroll(e){
	const scrollToTop = window.scrollY > 300;
	if(scrollToTop){
		window.scrollTo({top:0,behavior:"smooth"});
	}//Else scroll to bottom
};

//Mangler bl.a. throttling
function scrollEvent(e){
const rotateArrow = window.scrollY > 300;
	if(rotateArrow){
		arrowElement.classList.remove('arrowDown');
		arrowElement.classList.add('arrowUp');
		return;
	}else {
		arrowElement.classList.remove('arrowUp');
		arrowElement.classList.add('arrowDown');
	};
};

